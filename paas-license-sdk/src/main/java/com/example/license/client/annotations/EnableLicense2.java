package com.example.license.client.annotations;

import com.example.license.client.config.LicenseWebMvcConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
@Import({LicenseWebMvcConfig.class})
public @interface EnableLicense2 {
}
