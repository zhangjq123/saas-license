package com.example.license.client.controller;


import com.example.license.client.domain.LicenseVerify;
import com.example.license.client.domain.LicenseVerifyParam;
import com.example.license.client.utils.LicenseManagerHolder;
import com.example.license.common.utils.ResultBean;
import de.schlichtherle.license.LicenseContent;
import de.schlichtherle.license.LicenseManager;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/license")
public class licIdxController {

    /**
     * 证书subject
     */
    @Value("${license.subject:license_sub}")
    private String subject;

    /**
     * 公钥别称
     */
    @Value("${license.publicAlias:publicCert}")
    private String publicAlias;

    /**
     * 访问公钥库的密码
     */
    @Value("${license.storePass:abc@123}")
    private String storePass;

    /**
     * 证书生成路径
     */
    @Value("${license.licensePath:F:/keytool/license.lic}")
    private String licensePath;

    /**
     * 密钥库存储路径
     */
    @Value("${license.publicKeysStorePath:F:/keytool/publicCerts.keystore}")
    private String publicKeysStorePath;

    @RequestMapping(value = "/getInfo")
    public Map getInfo() {
        Map<String,Object> map =new HashMap<>();
        map.put("title","XXXXXX平台");
        map.put("url","https://www.baidu.com");
        return map;
    }

    @RequestMapping(value = "/reset")
    public ResultBean<?> reset() {
        if(StringUtils.isNotBlank(licensePath)){
            System.out.println("++++++++ 开始安装证书 ++++++++");
            LicenseVerifyParam param = new LicenseVerifyParam();
            param.setSubject(subject);
            param.setPublicAlias(publicAlias);
            param.setStorePass(storePass);
            param.setLicensePath(licensePath);
            param.setPublicKeysStorePath(publicKeysStorePath);
            LicenseVerify licenseVerify = new LicenseVerify();
            //安装证书
            try {
                licenseVerify.install(param);
                System.out.println("++++++++ 证书安装成功 ++++++++");
            } catch (Exception e) {
                return ResultBean.fail("++++++++ 证书重新安装失败 ++++++++");
            }
        }
        return ResultBean.ok("++++++++ 证书重新安装成功 ++++++++");
    }

    @RequestMapping(value = "/getTime")
    public ResultBean<?>  getTime() {
        LicenseManager licenseManager = LicenseManagerHolder.getInstance(null);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            LicenseContent licenseContent = licenseManager.verify();
            Date date = licenseContent.getNotAfter();
            //return ResultBean.ok("++++++++ "+format.format(date)+" ++++++++");
            return ResultBean.ok(dateFormat.format(date));
        } catch (Exception e) {
            return ResultBean.fail("++++++++ 获取失效时间失败 ++++++++");
        }
    }

    @RequestMapping("/upload")
    public ResultBean<?> handleFileUpload(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return ResultBean.fail("++++++++ 文件为空 ++++++++");
        }
        File dest = new File(licensePath);
        // 检测是否存在目录
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            file.transferTo(dest);
            return ResultBean.ok("++++++++ 上传成功 ++++++++");
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResultBean.fail("++++++++ 上传失败 ++++++++");
    }

    @RequestMapping(value = "/index")
    public String index() {
        return "hello world!";
    }

}