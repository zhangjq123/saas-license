package com.example.license.client.config;

import com.example.license.client.event.LicenseCheckListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(
        basePackages = {
                "com.example.license.client.controller",
                "com.example.license.client.filter"
        }
)
public class LicenseWebMvcConfig {

        public LicenseWebMvcConfig() {
                System.out.println("wo shi pei zhi lei");
        }

        @Bean
        public LicenseCheckListener licenseCheckListener(){
                return new LicenseCheckListener();
        }

}
